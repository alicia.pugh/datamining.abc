##Activation des librairies
setwd("~/Documents/Fouille/Fouille2024/ProjetHealth/national+health+and+nutrition+health+survey+2013-2014+(nhanes)+age+prediction+subset") #a modifier pour appliquer à l'utilisateur
library(tidyverse)  
library(dplyr)

read.csv("health.csv", sep=",")  %>% head #colonnes "dbl"
tb <- read_csv('health.csv')  %>%
  mutate_if(is.character, factor) #on tranforme les colonnes au format "caractère" en format "fctr"=factor
tb
dim(tb) #2278 lignes; 10 colonnes

sum(is.na(tb)) #pas de valeurs manquantes dans le tableau, toutes les cases sont remplies
sum(duplicated(tb)) #pas de duplicats

tb = tb %>%
  rename(Age = RIDAGEYR, Genre = RIAGENDR, Sport = PAQ605, BMI = BMXBMI, Glucose = LBXGLU, Diabete = DIQ010, Insuline = LBXIN) #on renomme certaines colonnes pour une lecture plus facile

tb = tb %>%
  mutate(Diabete=case_when(
    Diabete==1 ~ "diabetique",
    Diabete==2 ~ "non-diabetique",
    Diabete==3 ~ "borderline",
    TRUE ~ as.factor(Diabete)
    ))

tb = tb %>%
  mutate(Genre=case_when(
    Genre==1 ~ "homme",
    Genre==2 ~ "femme",
    TRUE ~ as.factor(Genre)
    ))

  tb = tb %>%
  mutate(Sport=case_when(
    Sport==1 ~ "sportif",
    Sport==2 ~ "non-sportif",
    Sport==7 ~ "no data",
    TRUE ~ as.factor(Sport)
    ))

write_csv(tb, "donnees_pre-traitees.csv")

