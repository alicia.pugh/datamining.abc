Scripts accompagnés de la documentation utilisateur permettant d'obtenir la matrice *individus-variables* qui servira pour le projet.

Les scripts n'utilisent que les données disponibles dans le répertoire `../data`. Chaque script fournit un fichier utilisé pour les parties suivantes de l'analyse.

**Je vous demande également de rédiger un peu de documentation pour cette partie. Il s'agit de remplacer ce README pour indiquer à l'utilisateur (rbarriot) comment faire pour obtenir votre matrice.**

A partir du fichier csv disponible dans le repositoire, nous voulons vérifier la complétude des données, retirer le numéro d'ID de chaque individu (puisque cette information n'apporte rien), et changer les noms des variables pour qu'ils soient plus commodes à utiliser lors de l'analyse. Le but est de créer une matrice de données qui est exploitable dans Knime, dans R, et en Python.

La première matrice obtenue contient les informations médicales de chaque individu. Il y a des variables qualitatives (groupe d'age, sexe, niveau de sportivité...) et des variables quantitatives (quantité de gluxose dans le sang, quantité d'insuline...) dans le premier fichier créé. Puisque certaines méthodes de classification n'acceptent que les variables quantitatives, nous avons également généré une matrice sans les variables qualitatives. Nous n'avons pas de méthode pour compenser la perte d'informations dû à ce filtrage.

Pour obtenir les deux fichiers, commencer par utiliser le script nommé "script_preparation.R", puis celui nommé "script_discretisation.R" sur le résultat du premier script.

