# Fouille de données sur l'état de santé nutritionnelle des adultes et des enfants aux États-Unis

Ce jeu de données contient les 6287 instances. Il contient 10 variables: SEQN-identifiant d'un patient, ces données personnelles (âge, genre), information sur l'activité physique (s'il y en a en semaine), et données sur la santé (si un patient estv diabétique, taux de glucose sanguin à jeun, résultat de OGTT, taux d'insuline).

# Objectifs

L’objectif de ce projet est de construire un modèle permettant de classifier des individus en tant que diabétiques ou non en fonction des autres variables.

# Résultats phares

Nous avons découvert que les classificateurs Naif Bayésien et par Analyse Linéaire Discriminante fonctionnent le mieux sur nos données. Les autres ont eu des résultats limités.

# Liens

-Données: https://archive.ics.uci.edu/dataset/887/national+health+and+nutrition+health+survey+2013-2014+(nhanes)+age+prediction+subset




