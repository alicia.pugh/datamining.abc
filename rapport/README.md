
# Contexte

Dans ce projet, nous avons sélectionné un ensemble de données élaboré pour évaluer l'état de santé nutritionnelle des adultes et des enfants aux États-Unis.
Le diabète est une maladie chronique qui se développe lorsque le pancréas ne produit pas suffisamment d'insuline ou lorsque l'organisme ne parvient pas à l'utiliser efficacement. L'insuline est une hormone régulatrice de la glycémie. Si le diabète n'est pas traité, il peut évoluer et entraîner des complications graves dans de nombreux systèmes corporels, notamment les nerfs et les vaisseaux sanguins.
En 2014, 8,5 % des adultes âgés de 18 ans et plus étaient atteints de diabète. En 2019, le diabète était la cause directe de 1,5 million de décès et 48 % de l’ensemble des décès dus au diabète sont survenus avant l’âge de 70 ans. De plus, 460 000 autres décès par maladie rénale ont été causés par le diabète et l’hyperglycémie est à l’origine d’environ 20 % des décès imputables à des maladies cardiovasculaires (1. Global Burden of Disease Collaborative Network. Global Burden of Disease Study 2019. Results. Institute for Health Metrics and Evaluation. 2020 (https://vizhub.healthdata.org/gbd-results/)).

Entre 2000 et 2019, les taux de mortalité due au diabète standardisés selon l’âge ont augmenté de 3 %. Dans les pays à revenu intermédiaire de la tranche inférieure, le taux de mortalité prématurée due au diabète a augmenté de 13 % 
(https://www.who.int/fr/news-room/fact-sheets/detail/diabetes).


# Analyse

L’objectif de ce projet est de construire un modèle permettant de classifier des individus en tant que diabétiques ou non en fonction des autres variables.
Pour juger de l’efficacité de chaque méthode on prend en compte:

- Complétude (trouver tous les patterns intéressants) et optimisation (trouver uniquement les patterns intéressants).
- Sensibilité/spécificité/précision
- Pertinence du/des patterns retrouvé(s)

# Conception

Le jeu de données est constitué de 6287 instances réparties sur 10 variables.
Les données ont été collectées à travers des entretiens, des examens physiques et des tests de laboratoire. 

Ce dataset comprend 10 variables:
- SEQN (ID);
- age_group (senior/non senior): les répondants agés de 65 ans et plus ont été étiquetés comme “senior” et tous les individus de moins de 65 ans comme “adulte”.
- RIDAGEYR (âge d’un répondant);
- RIAGENDR (genre d’un répondant: 1 - homme, 2 - femme);
- PAQ605 (permet de savoir si une personne fait de l’activité physique en semaine: 1 - oui, 2 - non);
- BMXBMI (index de la masse corporelle d’un individu);
- LBXGLU (taux de glucose dans le sang à jeun);
- DIQ010 (1 - diabétique, 2 - non diabétique, 3 - borderline/pré-diabète);
- LBXGLT (respondent’s oral): montre le résultat de Oral Glucose Tolerance Test* (concentration de glucose en mg/dL), avec le seuil minimal de détection (Lower Limit Of Detection, LLOD) de 2 mg/dL. Le test est effectué sur les répondants, hommes et femmes, >12 ans); 
- LBXIN (taux d’insuline dans le sang d’un individu): mesuré dans les répondants >12 ans.


Certaines variables ont été pré-traitées avant d’être déposées sur le repositoire en ligne. Par exemple, la variable continue “age” a déjà été résumée en une variable qualitative “age_group” à deux modalités. De même, le niveau d’activité d’un individu est stocké dans une variable qualitative à trois niveaux.
Pour certaines méthodes de classification, les données quantitatives continues sont idéales. En revanche, pour certaines autres, nous allons devoir établir une méthode pour les discrétiser en intervalles de valeurs.

Dans le jeu de données initial, 2,7% des individus sont notés diabétiques, et 7,6% pré-diabétiques.

Le jeu de données initial ne contient pas de données manquantes.


## Conception de matrice

Le pré-traitement des données et la conception de matrice a été possible grâce au logiciel RStudio qui a permis de trier les données.

Nous avons décidé de classer les individus en fonction de leur état de santé (diabétiques, non-diabétiques ou pré-diabète (borderline). Nous avons sélectionné les patients comme individus. Les variables disponibles étaient de type quantitatif (Age, BMI, Glucose, LBXGLT et Insuline) et qualitatif (ID, age_group, genre, sport, diabète). 

Il faut remarquer qu’on a du discrétiser les valeurs de certaines variables quantitatives pour pouvoir les utiliser dans méthodes de classement (ex. arbres de décision, réseau de neurones, etc). Les variables qui ont été modifiées sont:
| Age (années) | BMI (kg/m²) | Glucose (mg/dL) |LBXGLT - OGTT (mg/dL) |Insuline (µU/mL)|
| ------------ | ----------- |-----------------|----------------------|----------------|
| 10-20        | 12-18.5     |60-140           |40-604                |0.14-102.29     |
| 21-30        | 18.6-25     |141-199          |40-139                |0.10-3.5        |
| 31-40        | 25.1-30     |200-405          |140-199               |3.51-6.60       |
| 41-50        | 30.1-40     |                 |200-604               |6.61-10.12      |
| 51-60        | 40.1-71     |                 |                      |10.13-16.17     |
| 61-70        |             |                 |                      |16.18-34.86     |
| 71-80        |             |                 |                      |34.87-102.3     |

Les valeurs ont été discrétisées en fonction des seuils utilisés lors de leur interprétation médicale. Les seuils ont été récupérés sur le site du CDC, l’équivalent Américain du Ministère de la Santé.


## Classification avec KNIME

KNIME (Konstanz Information Miner) est un logiciel libre et open-source d’analyse de données. Il permet l’intégration de divers langages de programmation et outils ainsi que la création automatique de compte-rendus et construction et évaluation de différents modèles. 

Une réalisation d’une analyse avec KNIME nécessite 3 étapes consécutives:
- charger des données;
- construire des modèles;
- évaluer des performances.

Pour débuter une analyse, il faut mettre un premier nœud du workflow - File Reader, qui permet de charger les données d’intérêt. La configuration du nœud se fait en fonction du fichier analysé. En général, il y a plusieurs paramètres à modifier:
- délimiteur des colonnes
- lecture des ID des colonnes
- lecture des headers de colonnes etc.

Dans notre cas on a choisi la lecture des headers de colonnes. Le format a été détecté automatiquement.
Le File Reader doit être validé après une vérification du bon formatage du fichier. 

L’étape suivante consiste à construire des modèles, ce qui est réalisé par l’ajout des différents nœuds correspondants aux différents modèles utilisés. Dans le cadre de ce projet, on a tenté d’utiliser les classificateurs suivants:
- Arbre de décision (Decision Tree Learner);
- Bayésien naif (Naive Bayes Learner);
- Forêts aléatoires (Random Forest Learner);

L’insertion du Decision Tree Learner implique une nécessité de choisir une variable sur quelle l’analyse va être réalisée, une mesure de qualité (Gini index: réduit le biais au niveau du gain d’information, ou Gain ratio: mesure la fréquence avec laquelle un élément aléatoire de l’ensemble de données pourrait être mal classé), et une méthode d'élagage (Pruning method: no pruning ou MDL). 
Nous avons testé les deux mesures de qualité et les deux méthodes d’élagage. 

Dans Naive Bayes Learner on peut choisir une colonne de classification, une probabilité par défaut, un minimum et un seuil de la déviation standard. 

Les résultats de différentes combinaisons seront comparés et discutés dans la partie suivante “Résultats”.

Ensuite, un nœud “Predictor” est ajouté. Il correspond au nœud d’apprentissage choisi et prend en entrée un modèle construit et les données à classer.

L’étape d’évaluation des performances de modèles se fait avec X-Partitioner qui est placé à la sortie du File Reader. X-Partitioner sert à créer un jeu de données dont une partie va être un jeu d’apprentissage pour le nœud d’apprentissage, et l’autre partie va jouer un rôle d’un jeu de test pour le nœud de prédiction. 
Dans X-Partitioner il est possible de modifier le nombre de validation et de choisir un mode sampling: 
- linear (linéaire);
- random (aléatoire);
- stratified (stratifié).

Notre X-Partitioner prend toujours 10 comme le nombre de validations.
La colonne de classification est par défaut “diabete” et “ignore missing values” - True. 

Le dernier nœud, Scorer, a été utilisé pour comparer les performances de modèles utilisés.  

Le workflow utilisé:
![Workflow dans Knime](workflow_knime.png)


## Classification par Analyse Linéaire Discriminante
L’Analyse Linéaire Discriminante (LDA) est intéressante dans notre cas car les données sont toutes très semblables et effectuer une analyse qui maximise leur dispersion permet de mieux distinguer s’il existe des groupes inhérents aux données. Nous avons effectué cette analyse dans RStudio avec la librairie MASS.

La LDA ne peut être effectuée que sur des données quantitatives, donc nous avons pré-sélectionné les variables de type compatible (Age, BMI, Glucose, LBXGLT et Insuline). Puis, étant donné que les variables avaient des unités différentes, nous avons effectué la LDA normée et centrée. 

Pour estimer la qualité du modèle, nous avons utilisé une fonction produisant une matrice de confusion, ainsi que plusieurs statistiques globales et par variable.

## Classification par MLP
Enfin, nous avons effectué une classification par réseau de neurones Multi-layer Perceptron (MLP) pour classifier les individus. Cette analyse a été effectuée dans Python avec le package scikit-learn. Puisque cet outil ne permet que l’analyse sur des données quantitatives, nous avons effectué l’analyse avec le même jeu de données que la LDA sans les variables qualitatives. Nous avons ensuite effectué une deuxième classification utilisant les coordonnées des individus sur les 5 axes d’une ACP et sur les composantes linéaires de la LDA.

Pour la classification des individus n’utilisant que les données quantitatives, il a d’abord fallu séparer les individus en groupes d’entraînement et test. Pour ce faire nous avons sélectionné ⅔ des individus pour le jeu de données d'entraînement et utilisé le reste pour tester les capacités du modèle. Nous avons retiré la colonne de la classe à prédire. Ensuite, nous avons effectué la classification MLP. 

Pour estimer la qualité de la prédiction, dans les deux cas, une matrice de contingence ainsi qu’un calcul du taux d’erreur ont été utilisés.


# Réalisation

##  KNIME
Après avoir effectué une classification des données par arbre de décision (sur la capture d’écran le test est réalisé avec Linear sampling, Gini index et no pruning), le résultat est représenté sous forme d’un arbre suivant:
![Arbre dans Knime](tree_knime.png)

Parmi toutes les variables, les variables LBXGLT et Glucose sont les plus concluantes pour déterminer si un patient est diabétique. Il est donc logique que ces variables soient isolées comme la plus efficace pour distinguer les individus diabétiques des non-diabétiques.
Le tableau dessous représente les taux d’erreur pour chaque combinaison de X-Partitioner et Decision Tree Learner:

| Taux d'errer pour Decision Tree Learner (%) | Gain + No pruning |  Gain + MDL |  Gini + No pruning | Gini + MDL |
| ------------------------------------------- | ----------------- | ------------|-----------|------------|
|    Linear                                   |     3.524         | 2.632       |          2.632      | 2.632      |
|    Random                                   |     2.632         | 2.632       |          2.632      | 2.632      |

La structure du random forest obtenu est significativement plus complexe car souvent il n’y a qu’un seul individu classé sur chaque décision. En plus, une énorme variation est présente au sein de ces arbres car il y a souvent des différentes variables discriminantes.
Néanmoins, l’intérêt d’une forêt aléatoire est l’assemblage des résultats de plusieurs arbres.

Le dernier modèle a été réalisé avec Naive Bayes Predictor.

Les taux d’erreur obtenus pour la matrice de confusion pour chaque classificateur avec Diabète sont représentés dans le tableau dessous:

| Taux d'errer (%)  | Decision Tree Learner (moyenne) |  Random Forest Learner |        Naive Bayes Learner |
| ----------------  | ------------------------------- | -----------------------|--------------      |
|    Linear         |     2.855                       | 2.632                  |           2.632               | 
|    Random         |     2.632                       | 2.193                  |           3.947               | 

On remarque que Random Sampling montre des meilleurs résultats avec Decision 
Tree Learner (2.632%) et Random Forest Learner (2.193%) par rapport à ceux obtenus avec Linear Sampling, 2.855% et 2.632% respectivement. Cela est attendu car Random Sampling est plus adapté aux grands ensembles de données. Toutefois, dans les deux cas le taux d’erreur est significativement inférieur à 5%.

Il faut également faire attention au fait que dans les deux cas (Decision Tree Learner et Random Forest Learner) tous les individus ont été classés comme sains. Leur Cohen’s kappa est égal à 0 dans tous les cas, c’est à dire que les individus auraient pu être classés de façon entièrement aléatoire.

Cependant, ce n’est pas le cas pour Naive Bayes Predictor. Malgré son taux d’erreur légèrement plus élevé en comparaison avec les deux autres modèles, il permet de mieux distinguer les individus sains et diabétiques. Cette méthode a un Cohen’s kappa de 0.113%, qui est une valeur faible mais néanmoins plus prometteuse que celle des deux autres méthodes.

## LDA
Avant de commencer cette analyse, nous avons effectué une ACP pour comparer la distribution des données en fonction des deux méthodes. Théoriquement, cette analyse était intéressante à réaliser car elle pourrait permettre une distinction des groupes inhérents aux données. Cependant, on remarque qu'il n'y a pas de clusters formés, donc pas de regroupements visibles de chaque catégorie (diabétiques, non-diabétiques, borderline).
Le chevauchement significatif des groupes signifie une faible capacité du modèle à les discriminer. Cette conclusion est également applicable à la projection de la LDA.

![projection de l’ACP à gauche; projection de la LDA à droite](ACP_LDA_comparaison.png)

Nous avons ensuite cherché à estimer la justesse de cette méthode de classification. La distribution des données suivant l’analyse est représentée ci-dessous. Un point sur le graphique correspond à une observation. La forme d’un point correspond à une classe connue et la couleur correspond à une classe prédite. On n’a toujours pas été capable de bien séparer les groupes. 

![Projection de la LDA: predictions vs connues](LDA_prediction.png)

La matrice de confusion obtenue est présentée en-dessous:

![Matrice de confusion de la LDA](mat_confusion_lda.png)

On remarque que ce modèle est capable de caractériser les classes et ne prédit pas que tous les individus sont sains. Malgré la mauvaise séparation de groupes à l'œil nu, le modèle est précis à 94,73%. Le Cohen’s kappa de 0,05 est moins bon que celui du classificateur naïf bayésien, ce qui n’est pas prometteur, mais le fait que cette méthode produise des résultats est intéressant.

Le “No Information Rate” est la proportion du jeu de données qui appartient à la classe la plus peuplée. Cette proportion est comparée au taux de justesse (accuracy) pour déterminer si le modèle de classification est plus performant qu’une distribution aléatoire. Puisqu’ici le taux d’accuracy est plus bas que le No Information Rate, le modèle est légèrement moins efficace qu’un classificateur aléatoire. Cette statistique fait écho aux résultats des arbres de décision et forêts aléatoires qui ont classifié tous les individus comme étant sains.


## MLP
La classification par réseau de neurones étant réputée pour sa capacité à classifier même les données les plus complexes, nous nous attendions à ce que cette méthode soit capable de différencier les classes avec un taux d’erreur faible. En effet, la méthode a un faible taux d’erreur, mais c’est principalement dû au fait que la méthode classifie tous les individus comme étant sains, et qu’il n’y a pas beaucoup d’individus atteints dans le jeu de données.

![Matrice de confusion du MLP sur les données quantitatives](mlp_donnees_quanti.png)

Etant donné que la réalisation du LDA a montré à quel point les données étaient similaires, nous avons tenté la classification par MLP des individus en fournissant les coordonnées des individus sur les axes artificiels créés lors de la LDA et l’ACP exploratoire. Le but était d’utiliser un jeu de données où la dispersion des individus était maximisée, dans l’espoir que le MLP puisse mieux distinguer les caractéristiques des classes. Bien que potentiellement source de plus d’erreurs, nous espérions obtenir des résultats plus concluants.

![Matrice de confusion du MLP sur les coordonnees des autres analyses](mlp_coordonnees.png)

Malheureusement, même avec les données transformées, le MLP prédisait que tous les individus étaient sains. La différence entre les deux taux d’erreur est attribuée au fait que lors du partitionnement des données en jeu d’apprentissage et de test, le nombre d’individus sains ou malades variait aléatoirement.


# Discussion

Ce projet nous a permis de travailler avec les données réelles brutes fournies par National Health and Nutrition Health Survey. 
Lors de la réalisation de ce projet nous avons approfondi nos connaissances de la gestion des données sur R ainsi que les compétences de l’utilisation du logiciel KNIME. L’opportunité d’utiliser le package sklearn était également intéressant.

Nous avons pu essayer d’utiliser les différents algorithmes d’apprentissage supervisé tels que l’arbre de décision, le classificateur naïf bayésien et la forêt aléatoire. Il était intéressant de voir quel algorithme serait le plus adapté pour nos données étant un mélange de variables qualitatives et quantitatives et le problème qu’on a choisi (classification des individus en trois catégories: diabétiques, non-diabétiques et pré-diabète (borderline)). 
# Bilan et perspectives

Les problèmes survenus lors de la classification sont principalement attribués au faible taux d’individus diabétiques ou pré-diabétiques dans le jeu de données. Étant donné leur faible nombre, plusieurs classificateurs ont calculé que le taux d’erreur serait plus bas s’ils classaient tous les individus comme étant sains. Nous avons voulu conserver l’intégrité du jeu de données donc nous l’avons laissé tel quel, mais pour des analyses futures nous pourrions par exemple enrichir le jeu de données en individus atteints, pour qu’ils soient plus faciles à détecter par les diverses méthodes de classification.

Étant donné les limitations des différents modèles, certains classificateurs n’ont pu accepter que 5 variables du jeu de données. Dans la littérature générale sur la prédiction de l’état de santé d’individus atteints du diabète, les critères les plus importants sont le BMI, le tour de taille, l'âge et la tension artérielle ((1)Semerdjian J, Frank S. An Ensemble Classifier for Predicting the Onset of Type II Diabetes. ArXiv e-prints. 2017. 1708.07480.;(2) Yu W, Liu T, Valdez R, Gwinn M, Khoury MJ. Application of support vector machine modeling for prediction of common diseases: the case of diabetes and pre-diabetes. BMC Med Inf Decis Making. 2010; 10(1):16. https://doi.org/10.1186/1472-6947-10-16.) . Nous avons inclus deux de ces variables dans notre jeu de données, ainsi que d’autres valeurs utilisées pour diagnostiquer le diabète, mais nous avons tout de même un petit nombre de variables sur lesquelles nos modèles peuvent s’appuyer. Nous n’avons pas cherché à augmenter le nombre de variables pour les mêmes raisons que nous n’avons pas enrichi le jeu de données en individus diabétiques, mais ce serait une autre piste à suivre pour des analyses futures sur ce jeu de données.

Il était étonnant que autant de méthodes soient incapables de distinguer les individus sains des individus malades ou pré-diabétiques. En effet, lors de la conception du projet nous nous attendions à ce que toutes les méthodes fonctionnent, et que leur comparaison se ferait principalement sur leur efficacité. Cependant, les résultats obtenus mettent en évidence un problème central de la médecine: étant donné que la grande majorité des individus sont sains, comment distinguer avec certitude les individus qui ne le sont pas? Dans le cadre de notre analyse, même en effectuant de la classification basée sur les résultats de critères de diagnostic, plusieurs modèles étaient simplement incapables de distinguer les personnes saines des personnes atteintes. Ce résultat met en évidence la difficulté du travail de médecin, la complexité des critères utilisés pour un diagnostic, et l’importance de la complétude de méthodes de classification dans un cadre médical.


# Gestion du projet
Pendant la réalisation de ce projet on a rencontré certaines difficultés assez classiques comme la gestion du temps et les problèmes techniques.
D’abord, on a commencé à réaliser le projet légèrement plus tard qu’il était initialement prévu puisque notre dataset du début n’avait pas été cohérent et on a pris  du retard pour en choisir un autre. 
On a dû également travailler majoritairement depuis chez soi à cause des plannings universitaires et personnels variés.
Les problèmes techniques ont été aussi rencontrés surtout du côté de Anastasiia, par exemple, un package “caret” de R qu’elle n’a pas arrivé à installer et KNIME qui fonctionnait mieux sur l’ordinateur de Alicia. 


![Diagramme de Gantt](gantt_250424.png)





