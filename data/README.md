Les données proviennent d'une étude effectuée par un groupe associé au département de Santé du gouvernement des Etats Unis. L'étude a été nommé le NHANES (National Health and Nutrition Examination Survey) et effectuée entre 2013 et 2014. Chaque instance correspond à un individu ayant répondu au questionnaire. Le questionnaire a collecté un très grand nombre d'informations sur les individus choisis: 131 variables dans le jeu de données de départ.

Pour réduire la taille du jeu de données utilisé, nous avons commencé à partir du jeu de données présent dans le Machine Learning Repository. Ce jeu de données consiste d'environ 6 mille instances et comprend 10 variables. Il est disponible à l'addresse suivante: 
https://archive.ics.uci.edu/dataset/887/national+health+and+nutrition+health+survey+2013-2014+(nhanes)+age+prediction+subset
NA,NA. (2023). National Health and Nutrition Health Survey 2013-2014 (NHANES) Age Prediction Subset. UCI Machine Learning Repository. https://doi.org/10.24432/C5BS66.

